<?php namespace RTcustom\Analytics;

use Illuminate\Support\ServiceProvider;
use Google_Client;
use Config;
use Setting;

class AnalyticsServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->bind('analytics', function ($app) {

            $googleApiHelper = $this->getGoogleApiHelperClient();

            $analytics = new Analytics($googleApiHelper, Setting::get('advanced.analytics.siteId'));

            return $analytics;
        });
    }

    /**
     * Return a GoogleApiHelper with given configuration.
     *
     * @return GoogleApiHelper
     *
     * @throws \Exception
     */
    protected function getGoogleApiHelperClient()
    {
        $this->guardAgainstMissingP12();

        $client = $this->getGoogleClient();

        $googleApiHelper = (new GoogleApiHelper($client, app()->make('Illuminate\Contracts\Cache\Repository')))
            ->setCacheLifeTimeInMinutes(Setting::get('advanced.analytics.cacheLifetime'))
            ->setRealTimeCacheLifeTimeInMinutes(Setting::get('advanced.analytics.realTimeCacheLifetimeInSeconds'));

        return $googleApiHelper;
    }

    /**
     * Throw exception if .p12 file is not present in specified folder.
     *
     * @throws \Exception
     */
    protected function guardAgainstMissingP12()
    {
        if (!\File::exists(storage_path(Setting::get('advanced.analytics.certificatePath')))) {
            throw new \Exception("Can't find the .p12 certificate in: ".storage_path(Setting::get('advanced.analytics.certificatePath')));
        }
    }

    /**
     * Get a configured GoogleClient
     *
     * @return Google_Client
     */
    protected function getGoogleClient()
    {
        $client = new Google_Client(
            [
                'oauth2_client_id' => Setting::get('advanced.analytics.clientId'),
                'use_objects' => true,
            ]
        );
        
        $client->setClassConfig('Google_Cache_File', 'directory', storage_path('app/analytics-cache'));

        $client->setAccessType('offline');

        $client->setAssertionCredentials(
            new \Google_Auth_AssertionCredentials(
                Setting::get('advanced.analytics.serviceEmail'),
                ['https://www.googleapis.com/auth/analytics.readonly'],
                file_get_contents(storage_path(Setting::get('advanced.analytics.certificatePath')))
            )
        );

        return $client;
    }
}
